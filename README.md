<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.50.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.50.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_eks_cluster.ims-eks](https://registry.terraform.io/providers/hashicorp/aws/4.50.0/docs/resources/eks_cluster) | resource |
| [aws_eks_node_group.worker-node-group](https://registry.terraform.io/providers/hashicorp/aws/4.50.0/docs/resources/eks_node_group) | resource |
| [aws_iam_role.eks-iam-role](https://registry.terraform.io/providers/hashicorp/aws/4.50.0/docs/resources/iam_role) | resource |
| [aws_iam_role.workernodes](https://registry.terraform.io/providers/hashicorp/aws/4.50.0/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly](https://registry.terraform.io/providers/hashicorp/aws/4.50.0/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly-EKS](https://registry.terraform.io/providers/hashicorp/aws/4.50.0/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.AmazonEKSClusterPolicy](https://registry.terraform.io/providers/hashicorp/aws/4.50.0/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy](https://registry.terraform.io/providers/hashicorp/aws/4.50.0/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy](https://registry.terraform.io/providers/hashicorp/aws/4.50.0/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.EC2InstanceProfileForImageBuilderECRContainerBuilds](https://registry.terraform.io/providers/hashicorp/aws/4.50.0/docs/resources/iam_role_policy_attachment) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_eks_instance_type"></a> [eks\_instance\_type](#input\_eks\_instance\_type) | Choose Instance type of EKS | `string` | `"t3.medium"` | no |
| <a name="input_eks_name"></a> [eks\_name](#input\_eks\_name) | Name of the eks CLuster | `string` | `"abalfazl"` | no |
| <a name="input_eks_subnet_id_1"></a> [eks\_subnet\_id\_1](#input\_eks\_subnet\_id\_1) | which VPC to choose | `string` | `"subnet-0dc849630b41dc3ea"` | no |
| <a name="input_eks_subnet_id_2"></a> [eks\_subnet\_id\_2](#input\_eks\_subnet\_id\_2) | which VPC to choose | `string` | `"subnet-035f0011c6a3870f3"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster_ca_certificate"></a> [cluster\_ca\_certificate](#output\_cluster\_ca\_certificate) | CA Certificate of EKS Cluster |
| <a name="output_cluster_name"></a> [cluster\_name](#output\_cluster\_name) | Name of EKS Cluster |
| <a name="output_endpoint"></a> [endpoint](#output\_endpoint) | Endpoint of EKS Cluster |
<!-- END_TF_DOCS -->