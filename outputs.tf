output "endpoint" {
  description = "Endpoint of EKS Cluster"
  value       = try(resource.aws_eks_cluster.ims-eks.endpoint, "")
}

output "cluster_ca_certificate" {
  description = "CA Certificate of EKS Cluster"
  value       = try(resource.aws_eks_cluster.ims-eks.certificate_authority[0].data, "")
}

output "cluster_name" {
  description = "Name of EKS Cluster"
  value       = try(resource.aws_eks_cluster.ims-eks.name, "")
}