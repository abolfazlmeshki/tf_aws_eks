variable "eks_name" {
  type        = string
  default     = "abalfazl"
  description = "Name of the eks CLuster"
}

variable "eks_subnet_id_1" {
  type        = string
  default     = "subnet-0dc849630b41dc3ea"
  description = "which VPC to choose"
}

variable "eks_subnet_id_2" {
  type        = string
  default     = "subnet-035f0011c6a3870f3"
  description = "which VPC to choose"
}

variable "eks_instance_type" {
  type        = string
  default     = "t3.medium"
  description = "Choose Instance type of EKS"
}